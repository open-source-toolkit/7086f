# Qt封装的spdlog日志库

## 简介

本仓库提供了一个基于Qt封装的spdlog日志库资源文件。该日志库支持基本的日志记录功能，并且具备多线程安全特性。通过使用spdlog库，您可以轻松地在每日日志和循环日志中记录日志内容。spdlog库直接使用头文件，无需额外的cpp文件，方便后续的进一步开发和定制。

## 功能特点

- **多线程安全**：使用spdlog的多线程模式，确保日志记录在多线程环境下安全可靠。
- **每日日志**：自动生成每日日志文件，存储在单独的文件夹中。当文件数量超过30个时，自动删除之前的日志文件。
- **循环日志**：在固定的文件夹中生成日志文件，每个文件大小限制为5MB。最多存储10个文件，存储满后会向之前的文件中追加日志数据。
- **头文件使用**：spdlog库直接使用头文件，无需编译额外的cpp文件，方便集成和扩展。

## 使用方法

1. **克隆仓库**：
   ```bash
   git clone https://github.com/your-repo/qt-spdlog.git
   ```

2. **集成到项目**：
   将仓库中的头文件和资源文件集成到您的Qt项目中。

3. **配置日志路径**：
   根据您的需求，配置每日日志和循环日志的存储路径。

4. **记录日志**：
   使用提供的接口记录日志信息。

## 示例代码

```cpp
#include "spdlog_wrapper.h"

int main() {
    // 初始化日志库
    SpdlogWrapper::initLogger();

    // 记录日志
    SPDLOG_INFO("This is an info message.");
    SPDLOG_WARN("This is a warning message.");
    SPDLOG_ERROR("This is an error message.");

    return 0;
}
```

## 依赖

- Qt 5.x 或更高版本
- spdlog 1.x 或更高版本

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

## 贡献

欢迎提交Issue和Pull Request，共同完善本项目。

## 联系

如有任何问题或建议，请联系 [your-email@example.com](mailto:your-email@example.com)。

---

希望这个日志库能够帮助您更方便地进行日志记录和管理！